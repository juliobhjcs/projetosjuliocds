﻿using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using AppHinovateste.Models;
using Java.Lang;

namespace AppHinovateste
{
    internal class ListaAdapter : BaseAdapter
    {
        private MainActivity mainActivity;
        private List<ClasseOficina> listaoficina;
       
        public ListaAdapter(MainActivity mainActivity, List<ClasseOficina> listaoficina)
        {
            this.mainActivity = mainActivity;
            this.listaoficina = listaoficina;
        }

        public override int Count => listaoficina.Count;

        public override Object GetItem(int position)
        {
            return listaoficina[position].Id;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            Context context;
            View root = convertView ?? mainActivity.LayoutInflater.Inflate(Resource.Layout.Listaoficinas, parent, false);

          
            TextView txtnome = root.FindViewById<TextView>(Resource.Id.txtnome);
            TextView txttelefone1 = root.FindViewById<TextView>(Resource.Id.txttelefone1);
            TextView txttelefone2 = root.FindViewById<TextView>(Resource.Id.txttelefone2);
            TextView txtendereco = root.FindViewById<TextView>(Resource.Id.txtendereco);
            TextView txtdescricao = root.FindViewById<TextView>(Resource.Id.txtdescricao);
            Button btndetalhes = root.FindViewById<Button>(Resource.Id.btndetalhes);

            // Convertendo imagem em bytes
            ImageView imgfoto = root.FindViewById<ImageView>(Resource.Id.imgfoto);
            Bitmap bitmap = Util.ConverterBase64EmBitmap(listaoficina[position].Foto);
            imgfoto.SetImageDrawable(Util.GetRoundedBitmapDrawable(mainActivity.Resources, bitmap));

          
            txtnome.Text = listaoficina[position].Nome;
            txttelefone1.Text = listaoficina[position].Telefone1;
            txttelefone2.Text = listaoficina[position].Telefone2;
            txtendereco.Text = listaoficina[position].Endereco;
            txtdescricao.Text = listaoficina[position].DescricaoCurta;

            btndetalhes.Text = txtnome.Text + " " + "Ver detalhes";

            btndetalhes.Click += delegate
            {
                Intent intent = new Intent(mainActivity, typeof(ActivityDetalhes));
                intent.PutExtra("nome", root.FindViewById<TextView>(Resource.Id.txtnome).Text);
                intent.PutExtra("foto", bitmap);  
                intent.PutExtra("endereco", root.FindViewById<TextView>(Resource.Id.txtendereco).Text);
                intent.PutExtra("telefone1", root.FindViewById<TextView>(Resource.Id.txttelefone1).Text);
                intent.PutExtra("telefone2", root.FindViewById<TextView>(Resource.Id.txttelefone2).Text);
                intent.PutExtra("email", root.FindViewById<TextView>(Resource.Id.txtemail).Text);
                intent.PutExtra("descricao", root.FindViewById<TextView>(Resource.Id.txtdescricao).Text);
                root.Context.StartActivity(intent);
            };

            return root;
        }

    
    }
}