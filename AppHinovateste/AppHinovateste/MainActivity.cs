﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using AppHinovateste.ConexaoWebApi;
using AppHinovateste.Models;
using System.Collections.Generic;
using System;
using Android.Content;
using Android.Views;
using Plugin.Connectivity;

namespace AppHinovateste
{
    [Activity(Label = "Oficinas", Theme = "@style/Theme.AppCompat.Light.NoActionBar", MainLauncher = true, Icon = "@drawable/iconeprincipal")]
    public class MainActivity : AppCompatActivity
    {
        Button btnbuscaroficinas;
        ListView listaoficinas;
        Conexao conectar = new Conexao();
        ClasseRetornoOficina oficinasretorno = new ClasseRetornoOficina();
        List<ClasseOficina> listaoficina = new List<ClasseOficina>();

        EditText editcodigo;
        LinearLayout container;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layoutOficina);


            listaoficinas = FindViewById<ListView>(Resource.Id.listaoficinas);
            editcodigo = FindViewById<EditText>(Resource.Id.editcodigo);
            btnbuscaroficinas = FindViewById<Button>(Resource.Id.btnbuscaroficinas);
          


            btnbuscaroficinas.Click += Btnbuscaroficinas_Click1;

        }
        private void Btnbuscaroficinas_Click1(object sender, EventArgs e)
        {
            try
            {
                // Verificando se o campo código contem algum valor. 
                if(editcodigo.Text == "" || CrossConnectivity.Current.IsConnected == false)
                {
                    Toast.MakeText(this, "Entre com o código correto ou verifique sua conexão com a internet.", ToastLength.Long).Show();
                }
                else
                {           
                // Get Oficinas
                var buscarlistadeoficinas = conectar.GET<ClasseRetornoOficina>(oficinasretorno, editcodigo.Text);

                if (buscarlistadeoficinas != null) // Se o retorno teve sucesso
                {
                    listaoficina = buscarlistadeoficinas.listaOficinas; // instancia de lista oficina recebe retorno
                    ListaAdapter adapter = new ListaAdapter(this, listaoficina); // Adapter para criar uma lista de objetos
                    listaoficinas.Adapter = adapter;
                   
                  
                       
                }
                }
            }
            catch (System.Exception)
            {


            }
        }
        public override void OnBackPressed()
        {
            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
            alert.SetTitle("Fechar?");
            alert.SetMessage("Deseja realmente sair?");

            alert.SetPositiveButton("SIM", (senderAlert, args) =>
            {
                Toast.MakeText(this, "OK, Volte logo!", ToastLength.Short).Show();
                var activity = (Activity)this;
                activity.FinishAffinity();

            });
            alert.SetNegativeButton("NÃO", (senderAlert, args) =>
            {
                Toast.MakeText(this, "OK!", ToastLength.Short).Show();

            });

            Dialog dialog = alert.Create();
            dialog.Show();
        }

    }
}