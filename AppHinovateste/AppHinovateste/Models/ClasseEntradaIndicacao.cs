﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppHinovateste.Models
{
   public class ClasseEntradaIndicacao 
    {
      
        public string Remetentes { get; set; }
        public string Copias { get; set; }
        public ClasseIndicacao ClasseIndicacao { get; set; }
    }
}