﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace AppHinovateste
{
    [Activity(Label = "Activityindicacao", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class Activityindicacao : AppCompatActivity
    {
        TextView txtindicacao;
        Button btnfim;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layoutIndicacao);

            txtindicacao = FindViewById<TextView>(Resource.Id.txtindicacao);
            btnfim = FindViewById<Button>(Resource.Id.btnfim);

            txtindicacao.Text = "INDICAÇÃO ENVIADA" + "\n" + "COM SUCESSO!!!";

            btnfim.Click += Btnfim_Click;

        }

        private void Btnfim_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
        }
    }
}