﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Graphics.Drawable;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace AppHinovateste
{
    // classe para converter string em bitmap
    public static class Util
    {
        public static Bitmap ConverterBase64EmBitmap(string stringBase64)
        {
            if (!string.IsNullOrWhiteSpace(stringBase64))
            {
                byte[] decodedBytes = Base64.Decode(stringBase64, Base64Flags.Default);
                return BitmapFactory.DecodeByteArray(decodedBytes, 0, decodedBytes.Length);
            }

            return null;
        }

        public static RoundedBitmapDrawable GetRoundedBitmapDrawable(Resources resources, Bitmap bitmap)
        {
            RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.Create(resources, bitmap);
            roundedBitmapDrawable.Circular = true;
            roundedBitmapDrawable.SetAntiAlias(true);

            return roundedBitmapDrawable;
        }
    }
}