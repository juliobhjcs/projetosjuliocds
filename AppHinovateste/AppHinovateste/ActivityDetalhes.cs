﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Icu.Text;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using AppHinovateste.ConexaoWebApi;
using AppHinovateste.Models;


namespace AppHinovateste
{
    [Activity(Label = "ActivityDetalhes", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class ActivityDetalhes : AppCompatActivity
    {
        TextView txtnome, txttelefone1, txttelefone2, txtendereco, txtdescricao;
        ImageView imgfoto;
        Button btnretornar, btncompartilhar, btnenviar;
        EditText editemailindicado, editnomeindicado, editcontato, editplaca, editnomeassociado, edittelefone, editemail, editcpf, editdada, editcodigo;
        TextInputLayout nomeassociado, placa, contato, nomeindicado, emailindicado, telefone, email, cpf, data, codigo;
        Conexao conectar = new Conexao();
        ClasseIndicacao classeIndicacao = new ClasseIndicacao();
        ClasseEntradaIndicacao indicacao = new ClasseEntradaIndicacao();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layoutDetalhes);

             imgfoto = FindViewById<ImageView>(Resource.Id.imgfoto);
             txtnome = FindViewById<TextView>(Resource.Id.txtnome);
             txttelefone1 = FindViewById<TextView>(Resource.Id.txttelefone1);             
             txttelefone2 = FindViewById<TextView>(Resource.Id.txttelefone2);
             txtendereco = FindViewById<TextView>(Resource.Id.txtendereco);
             txtdescricao = FindViewById<TextView>(Resource.Id.txtdescricao);
             btncompartilhar = FindViewById<Button>(Resource.Id.btncompratilhar);
             btnretornar = FindViewById<Button>(Resource.Id.btnretornar);
             btnenviar = FindViewById<Button>(Resource.Id.btnenviar);

            // Editext
            editemailindicado = FindViewById<EditText>(Resource.Id.editemailindicado);           
            editnomeindicado = FindViewById<EditText>(Resource.Id.editnomeindicado);
            editcontato = FindViewById<EditText>(Resource.Id.editcontato);
            editcontato.AddTextChangedListener(new Mask(editcontato, "(##)####-####"));
            editplaca = FindViewById<EditText>(Resource.Id.editplaca);
            editplaca.AddTextChangedListener(new Mask(editplaca, "###-####"));
            editnomeassociado = FindViewById<EditText>(Resource.Id.editnomeassociado);
            edittelefone = FindViewById<EditText>(Resource.Id.edittelefone);
            edittelefone.AddTextChangedListener(new Mask(edittelefone, "(##)####-####"));
            editemail = FindViewById<EditText>(Resource.Id.editemail);
            editcpf = FindViewById<EditText>(Resource.Id.editcpf);
            editcpf.AddTextChangedListener(new Mask(editcpf, "###.###.###.##"));
            editdada = FindViewById<EditText>(Resource.Id.editdada);
            editcodigo = FindViewById<EditText>(Resource.Id.editcodigo);
          

            // imput layou
            nomeassociado = FindViewById<TextInputLayout>(Resource.Id.nomeassociado);
            placa = FindViewById<TextInputLayout>(Resource.Id.placa);
            contato = FindViewById<TextInputLayout>(Resource.Id.contato);
            nomeindicado = FindViewById<TextInputLayout>(Resource.Id.nomeindicado);
            emailindicado = FindViewById<TextInputLayout>(Resource.Id.emailindicado);
            telefone = FindViewById<TextInputLayout>(Resource.Id.telefone);
            email = FindViewById<TextInputLayout>(Resource.Id.email);
            cpf = FindViewById<TextInputLayout>(Resource.Id.cpf);
            data = FindViewById<TextInputLayout>(Resource.Id.data);
            codigo = FindViewById<TextInputLayout>(Resource.Id.codigo);



            DateTime dateatual = new DateTime(2018, 12, 10);
            editdada.Text = dateatual.ToString();

            Bitmap bitmap = (Bitmap)Intent.GetParcelableExtra("foto");
            imgfoto.SetImageBitmap(bitmap);

            txtnome.Text = Intent.GetStringExtra("nome");
            txttelefone1.Text = Intent.GetStringExtra("telefone1");
            txttelefone2.Text = Intent.GetStringExtra("telefone2");
            txtendereco.Text = Intent.GetStringExtra("endereco");
            txtdescricao.Text = Intent.GetStringExtra("descricao");

            btnretornar.Click += Btnretornar_Click;
            btncompartilhar.Click += Btncompartilhar_Click;
            btnenviar.Click += Btnenviar_Click;
            
        }

        private void Btnenviar_Click(object sender, EventArgs e)
        {
            try
            {
                var validaremail = validandoemail(editemail.Text);
                var validaremailassociado = validandoemail(editemailindicado.Text);
                if (editcodigo.Text == "" || editemailindicado.Text == "" || editnomeassociado.Text == "" || edittelefone.Text == "" || editemail.Text == "" || validaremail == false || validaremailassociado== false)
                {
                    Toast.MakeText(this, "Campos inválidos, certifique que todos os detalhes estejam corretos.", ToastLength.Long).Show();
                }
                else
                {
                    indicacao.Remetentes = editemail.Text;
                    
                  
                    //classeIndicacao.NomeAssociado = editnomeassociado.Text;
                    //classeIndicacao.NomeAmigo = editnomeindicado.Text;
                    //classeIndicacao.TelefoneAssociado = edittelefone.Text;
                    //classeIndicacao.TelefoneAmigo = editcontato.Text;
                    //classeIndicacao.EmailAssociado = editemail.Text;
                    //classeIndicacao.EmailAmigo = editemailindicado.Text;
                    //classeIndicacao.PlacaVeiculoAssociado = editplaca.Text;
                    //classeIndicacao.CpfAssociado = editcpf.Text;
                   

                    var conn = conectar.Post<ClasseEntradaIndicacao>(indicacao, editemail.Text);

                    if(conn != null)
                    {
                        Toast.MakeText(this, "Indicação enviada com sucesso!.", ToastLength.Long).Show();
                        Intent intent = new Intent(this, typeof(Activityindicacao));
                        StartActivity(intent);
                    }
                    else
                    {
                        Toast.MakeText(this, "Não foi possivel enviar sua indicação, verifique os dados e tente novamente.", ToastLength.Long).Show();
                    }


                }
            }
            catch (Exception)
            {

          
            }
        }

        private void Btncompartilhar_Click(object sender, EventArgs e)
        {
            nomeassociado.Visibility = ViewStates.Visible;
            placa.Visibility = ViewStates.Visible;
            contato.Visibility = ViewStates.Visible;
            nomeindicado.Visibility = ViewStates.Visible;
            emailindicado.Visibility = ViewStates.Visible;
            telefone.Visibility = ViewStates.Visible;
            email.Visibility = ViewStates.Visible;
            cpf.Visibility = ViewStates.Visible;
            data.Visibility = ViewStates.Visible;
            editdada.Enabled = false;
            codigo.Visibility = ViewStates.Visible;
            btnenviar.Visibility = ViewStates.Visible;
            Toast.MakeText(this, "Preencha os dados para compartilhar.", ToastLength.Long).Show();
        }

        private void Btnretornar_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
        }

        public bool validandoemail(string email)
        {
            return Android.Util.Patterns.EmailAddress.Matcher(email).Matches();
        }
    }
}